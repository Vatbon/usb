#include <iostream>
#include "libusb.h"
#include <stdio.h>
using namespace std;

void printdev(libusb_device *dev);

int main(){
	libusb_device **devs; // указатель на указатель на устройство,
	// используется для получения списка устройств
	libusb_context *ctx = NULL; // контекст сессии libusb
	int r;
	// для возвращаемых значений
	ssize_t cnt;
	// число найденных USB-устройств
	ssize_t i;
	// индексная переменная цикла перебора всех устройств
	// инициализировать библиотеку libusb, открыть сессию работы с libusb
	r = libusb_init(&ctx);
	if(r < 0){
		fprintf(stderr,"Ошибка: инициализация не выполнена, код: %d.\n", r);
		return 1;
	}
	// задать уровень подробности отладочных сообщений
	//libusb_set_option(ctx, LIBUSB_OPTION_LOG_LEVEL, 3);
	cnt = libusb_get_device_list(ctx, &devs);
	// получить список всех найденных USB- устройствcnt = libusb_get_device_list(ctx, &devs);
	if(cnt < 0){
		fprintf(stderr,"Ошибка: список USB устройств не получен.\n");
		return 1;
	}
	for(i = 0; i < cnt; i++) { // цикл перебора всех устройств
		printdev(devs[i]);
		printf("===========================================================\n");
		// печать параметров устройства
	}
	// освободить память, выделенную функцией получения списка устройств
	libusb_free_device_list(devs, 1);
	libusb_exit(ctx);
	// завершить работу с библиотекой libusb,
	// закрыть сессию работы с libusb
	return 0;
}
void printdev(libusb_device *dev){
	libusb_device_descriptor desc;
	// дескриптор устройства
	libusb_config_descriptor *config; // дескриптор конфигурации объекта
	int r = libusb_get_device_descriptor(dev, &desc);
	if (r < 0){
		fprintf(stderr,"Ошибка: дескриптор устройства не получен, код: %d.\n",r);
		return;
	}
	// получить конфигурацию устройства
	libusb_get_config_descriptor(dev, 0, &config);
	libusb_device_handle *handle;
	libusb_open(dev, &handle);
	unsigned char serial[256], manuf[256], prod[256];
	libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber, serial, 256);
	libusb_get_string_descriptor_ascii(handle, desc.iManufacturer, manuf, 256);
	libusb_get_string_descriptor_ascii(handle, desc.iProduct, prod, 256);
	libusb_close(handle);
	const libusb_interface *inter;
	const libusb_interface_descriptor *interdesc;
	inter = &config->interface[0];
	interdesc = &inter->altsetting[0];
	printf("Класс устройства: 0x%X\n", (int)desc.bDeviceClass);
	printf("Класс интерфейса: 0x%X\n", (int)interdesc->bInterfaceClass);
	printf("Идентификатор производителя: 0x%X\n", (int)desc.idVendor);
	printf("Идентификатор устройства: 0x%X\n", (int)desc.idProduct);
	//printf("%.2d %.2d %.4d %.4d %.3d | | | | | |\n",(int)desc.bNumConfigurations, (int)desc.bDeviceClass, desc.idVendor, desc.idProduct, (int)config->bNumInterfaces);
	printf("Производитель: %s\n", manuf);
	printf("Продукт: %s\n", prod);
	printf("Серийный номер: %s\n", serial);
	libusb_free_config_descriptor(config);
}
